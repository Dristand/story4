from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('magic/', views.magic, name='magic'),
    path('time/<str:timeChange>', views.time, name='time'),
    path('time', views.time, name='time'),
]