from django.shortcuts import render
import datetime

# Create your views here.

def index(request):
    return render(request, 'index.html')

def magic(request):
    return render(request, 'magic.html', {"magic": True})

def time(request, timeChange = "0"):
    timeChange = int(timeChange)

    # +7 to make the time format GMT+7
    time = datetime.datetime.now() + datetime.timedelta(hours = timeChange + 7)

    return render(request, 'time.html', {"finalTime" : time, "magic" : True})